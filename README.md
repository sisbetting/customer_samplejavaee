Example Java EE Client for SIS Connect Feed
===========================================

**ALL SAMPLE CODE IS PROVIDED ON AN "AS-IS" BASIS FOR GENERAL GUIDANCE, WITH AN EXISTING UNDERSTANDING OF JAVA EE APIs AND CONVENTIONS BEING PRESUMED. SIS CANNOT PROVIDE GENERAL SUPPORT FOR THE USE OF THE JAVA EE FRAMEWORK, WHICH IS DOCUMENTED AND DISCUSSED EXTENSIVELY ONLINE. PLEASE SEE https://bitbucket.org/sisbetting/customer_sampleclient 
FOR ADDITIONAL SAMPLES USING OTHER LANGUAGES AND/OR FRAMEWORKS**

This sample is currently provided only as a README because the way that Java EE apps are packaged, configured and deployed varies between application servers.

**Prerequisites**

Java EE 6 or later recommended. May work with Java EE 5.

Open/Oracle/IBM Java 8 or later recommended. May work with Java 6/7 - compatible at the code level but not tested.

Oracle Java (only) requires the JCE Unlimited Strength Policy Files to be installed from http://bit.ly/1beztkB otherwise SSL negotiation with our servers will fail.

SIS is not entitled to redistribute the JMS provider JAR file. It can be obtained from https://ibm.co/2qOQcgO (requiring a free IBM account to be created). Extract com.ibm.mq.allclient.jar from the distribution and include within your application's EAR file. Jms.jar is not required as it is part of the standard Java EE APIs.

**Configuration**

Below is an example message-driven bean to consume the feed: It assumes that the input queue has been set up under the JNDI path "jms/SisConnectQueue", with SSL connectivity configured within the app server. Note a single active MDB listener should be configured per MDB type, to maintain message order.

Check with your Account Manager or SIS Technical Contact on how to obtain connection credentials and client certificates.

**Consuming the Feed**

The feed can be consumed by a single process, or split for multiple consuming processes using message selectors.

The key to this is the messageSelector activation config property.

The full example below shows how to consume dog racing messages only; however, by removing/modifying the message selector repectively, no filtering at all, or different filtering, can be applied:

```java
        @ActivationConfigProperty(propertyName = "messageSelector",
                                  propertyValue = "categoryCode = 'DG'")
```

See the SIS Connect Wiki for available message properties that can be used to split feed processing: https://siswiki.sis.tv:8090/display/SC/High+Level+Overview

```java
@MessageDriven(mappedName="jms/SisConnectQueue", activationConfig =  {
        @ActivationConfigProperty(propertyName = "acknowledgeMode",
                                  propertyValue = "Auto-acknowledge"),
        @ActivationConfigProperty(propertyName = "destinationType",
                                  propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "messageSelector",
                                  propertyValue = "categoryCode = 'DG'")
})

public class DogRaceMessageListener implements MessageListener {
    private static final Logger LOG = LoggerFactory.getLogger(DogRaceMessageListener.class);
    private static final Logger DLC = LoggerFactory.getLogger("tv.sis.connect.DeadLetterLog");

    @Resource
    private MessageDrivenContext mdc;
    private volatile int sleep;

	/** 
     * Sample listener for dog-race related messages (see messageSelector). 
	 */
	@Override	
	public void onMessage(Message inMessage) {
	    try {
	    	LOG.debug("Received dog-race related message: {} with headers {}", inMessage, inMessage.getPropertyNames());
	        if (inMessage instanceof TextMessage) {
	            String message = ((TextMessage) inMessage).getText();
		        // Add processing here e.g. to convert to object form using jaxb and send to a local queue or database.
				// NB if a message cannot be processed for some reason to do with invalid headers or content, just log        
				// it as a dead letter without throwing an exception.
			
			} else {
				// Log as dead letter aka poison message and consume
				throw new ParseException("Message of unexpected type in dog race event feed: " + inMessage.getClass().getName());
	        }
	    } catch (Throwable e) {
			// Check for exceptions that indicate a problem with the payload/headers (add more as required)
			if (e instanceof ClassCastException || e instanceof ParseException || e instanceof NullPointerException) {
				DLC.Error("Missing/malformed message body or properties encountered", e.getMessage());
				DLC.Info("Properties: {}, Payload: {}", inMessage.getPropertyNames(), inMessage);
				return;
			}
			LOG.error("Unhandled exception occurred: {}. An external resource may be temporarily unavailable", e.getMessage());
			LOG.debug(e);
			// Wait a while (otherwise would redeliver immediately!) and ask container to roll back to the queue for redelivery 
			mdc.setRollbackOnly();
			sleep = Math.min(inMessage.getJMSRedelivered()? sleep * 2: 1000, 60000);
			LOG.debug("Sleeping for {}ms prior to redelivery", sleep);
			Thread.sleep(sleep);
		}
	}
}
```

**Error Handling**

Basic error handling is included in the sample code above, for:

- "Poison messages" - messages that are invalid and could therefore never be processed - which are diverted to a Dead Letter Log (endpoint configurable via logback.xml). While poison messages should never be encountered in production they need to be accommodated as a possibility, to prevent the queue becoming blocked
 
- Unhandled exceptions, resulting in transactional redelivery (assuming that there is some downstream problem with e.g. a web service or a database), logging an error - i.e. to be picked up by an external monitor - and delaying redelivery with exponential backoff